package sfedu.computergraphics.lab8

import javafx.scene.image.Image
import javafx.scene.image.ImageView

class ImageViewRepresentation : ImageView(), ImageRepresentation {

    override fun forImage(image: Image) {
        this.image = image;
    }
}