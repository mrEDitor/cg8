package sfedu.computergraphics.lab8

import javafx.scene.Node
import javafx.scene.chart.AreaChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.image.Image
import javafx.scene.layout.Region
import tornadofx.property


class HistogramRepresentation :
        AreaChart<Number, Number>(NumberAxis(0.0, 255.0, 16.0), NumberAxis()), ImageRepresentation {
    var redWeight by property<Double>(0.0)
    var greenWeight by property<Double>(0.0)
    var blueWeight by property<Double>(0.0)

    override fun forImage(image: Image) {
        val width = image.width.toInt();
        val height = image.height.toInt();
        val series = XYChart.Series<Number, Number>()
        val columns = DoubleArray(256)
        for (x in 0 until width) {
            for (y in 0 until height) {
                val color = image.pixelReader.getColor(x, y)
                var key = 0.0
                key += redWeight * color.red
                key += greenWeight * color.green
                key += blueWeight * color.blue
                columns[(255 * key).toInt()]++;
            }
        }
        val firstRawMoment = rawMoment(columns, width, height, 1)
        val firstCentralMoment = centralMoment(firstRawMoment, columns, width, height, 1)
        series.name = "u1 = " + firstCentralMoment
        for (x in 0..255) {
            val vertex = Data<Number, Number>(x, columns[x])
            vertex.node = Region()
            vertex.node.isVisible = false
            series.data.add(vertex)
        }
        data.setAll(series)
    }

    private fun rawMoment(columns: DoubleArray, width: Int, height: Int, order: Int): Double {
        var result = 0.0
        for (x in 0..255) {
            result += Math.pow(x.toDouble(), order.toDouble()) * columns[x]
        }
        result /= width * height
        return result
    }

    private fun centralMoment(firstRawMoment: Double, columns: DoubleArray, width: Int, height: Int, order: Int): Double {
        var result = 0.0
        for (x in 0..255) {
            result += Math.pow(x - firstRawMoment, order.toDouble()) * columns[x]
        }
        result /= width * height
        return result
    }
}