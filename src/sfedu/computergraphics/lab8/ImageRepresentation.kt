package sfedu.computergraphics.lab8

import javafx.scene.image.Image

interface ImageRepresentation {

    fun forImage(image : Image)
}