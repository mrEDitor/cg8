package sfedu.computergraphics.lab8

import javafx.collections.ObservableList
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.layout.Pane
import javafx.stage.FileChooser
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import tornadofx.App
import tornadofx.UIComponent
import tornadofx.View
import kotlin.properties.ReadOnlyProperty

class MainApp : App(Main::class)

class Main : View("CG-8") {
    override val root: Pane by fxml()
    val representations: ObservableList<ImageRepresentation> by fxid()

    fun open() {
        val chooser = FileChooser()
        val file = chooser.showOpenDialog(root.scene.window)
        if (file != null) {
            val dialog = Stage(StageStyle.UNDECORATED)
            dialog.initOwner(root.scene.window)
            dialog.initModality(Modality.APPLICATION_MODAL)
            val root: Parent = FXMLLoader.load(javaClass.getResource("Waiting.fxml"))
            val scene = Scene(root)
            dialog.scene = scene
            dialog.show()
            val image = Image(file.inputStream())
            representations.forEach { it.forImage(image) }
            dialog.close()
        }
    }
}